# TP - React JS

Le but de ce TP est de réaliser une page présentant des voitures de sport et leurs caractéristiques. Le résultat doit être le plus fidèle possible à cette maquette :

![](TP.png)

Chaque carte présente un modèle d'une marque. Un modèle n'a pas d'image: on doit donc afficher l'image de la 1ère variante du modèle.

Les caractéristiques sont listées sous chaque carte par variante : une variante peut être une année ou une finition.

Les caractéristiques sont : 
- le poids (fourni par l'API)
- la puissance (fourni par l'API)
- le rapport poids / puissance (à calculer)

Chaque caractéristique est affichée sous forme d'une barre d'une largeur de x%, x étant calculé par rapport aux maximums ci-dessous, qui correspondent à 100%:

```
const POWER_MAX = 1000; // max 1000hp
const WEIGHT_MAX = 2500; // max 2500kg
const RATIO_MAX = 1; // max 1kg/hp
```


L'image de fond est fournie : `hangar.jpg`, ainsi que l'icône de favori : `favicon.png`

L'entête comporte également une barre de recherche dans laquelle on peut taper le nom d'un modèle ou d'une marque : les modèles affichés seront filtrés en fonction.

Il comporte aussi un menu déroulant, qui permet d'afficher les 10 voitures les plus légères, les puissantes ou avec le rapport poids / puissance le plus bas.

## API de base

L'API de base permet de ramener 10 variantes aléatoirement : 

URL : http://cabe0232.odns.fr/webdev-api/sportscars

```
[
  {
    "id": 31,         // Identifiant de la marque
    "name": "PORSCHE" // Nom de la marque
    "models": [       // Liste des modèles de la marque
      {
        "id": 149,    // Identifiant du modèle 1
        "name": "996" // Nom du modèle
        "brandId": 31,// Identifiant de la marque (idem précédent)
        "cars": [     // Liste des variantes du modèle 1
          {
            "id": 605,             // Identifiant de la variante
            "name": "996 GT3 MK1", // Nom de la variante
            "power": 360.0,        // Puissance
            "weight": 1391.0,      // Poids
            "year": "1999",        // Année de sortie
            "imageUrl": "https://img.favcars.com/porsche/911-gt3/porsche_911-gt3_1999_photos_3.jpg" // URL de l'image à afficher
          }
        ]
      },
      {
        "id": 150,     // Identifiant du modèle 2
        "name": "panamera"
        "brandId": 31, 
        "cars": [      
          {
            "id": 622,
            "imageUrl": "https://img.favcars.com/porsche/panamera/porsche_panamera_2010_images_4.jpg",
            "name": "PANAMERA TURBO KIT USINE",
            "power": 540.0,
            "weight": 2022.0,
            "year": "2011"
          }
        ]
      }
    ]
  }
]
```


## API top 10

Les URL suivantes permettent de ramener les 10 voitures : 

- les plus légères: http://cabe0232.odns.fr/webdev-api/sportscars?sort=weight 
- les plus puissantes: http://cabe0232.odns.fr/webdev-api/sportscars?sort=power
- ayant le rapport poids / puissance le plus bas: http://cabe0232.odns.fr/webdev-api/sportscars?sort=ratio


## API recherche

L'URL suivante permet de faire une recherche :

- http://cabe0232.odns.fr/webdev-api/sportscars?search=texte

<texte> doit être remplacé par le texte recherché.

## Combiner les filtres

Combiner les 2 filtres ci-dessus pour effectuer une recherche comme celle-ci :

http://cabe0232.odns.fr/webdev-api/sportscars?search=texte&sort=weight

Pour cela il faut sauvegarder le dernier menu sélectionné et le texte saisi dans des _states_.

L'appel de l'API de recherche devrait s'effectuer avec _useEffect_ déclenché au changement d'un de ces 2 _states_. 

